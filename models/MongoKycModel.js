// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose')
var Schema = mongoose.Schema

// set up a mongoose model and pass it using module.exports
var model = mongoose.model(
  'KycModel',
  new Schema(
    {
      clientId: {
        type: String
      },
      blockPassID: {
        type: String,
        index: 1
      },

      identities: {
        type: Schema.Types.Mixed,
        default: {}
      },

      certs: {
        type: Schema.Types.Mixed,
        default: {}
      },

      status: {
        type: String,
        enum: ['waiting', 'inreview', 'approved'],
        default: 'waiting'
      },
      bpToken: {
        type: Schema.Types.Mixed,
        default: {}
      },
      bpProfile: {
        type: Schema.Types.Mixed
      }
    },
    { timestamps: true }
  )
)

module.exports = model
