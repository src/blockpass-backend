const { Duplex } = require('stream')
const mongoose = require('mongoose')
const Gridfs = require('mongoose-gridfs')

const GRIDFS_COLLECTION = 'kycAttachment'

let Attachment
let gfs
mongoose.connection.on('connected', () => {
  gfs = Gridfs({
    collection: GRIDFS_COLLECTION,
    model: 'Attachment',
    mongooseConnection: mongoose.connection
  })
  Attachment = gfs.model
})

function _bufferToStream(buffer) {
  const stream = new Duplex()
  stream.push(buffer)
  stream.push(null)
  return stream
}

function writeFile({ fileName, mimetype, metadata, fileBuffer }) {
  return new Promise((resolve, reject) => {
    Attachment.write(
      {
        filename: fileName,
        contentType: mimetype,
        metadata: metadata || {}
      },
      _bufferToStream(fileBuffer),
      function(error, createdFile) {
        if (error) return reject(error)
        resolve(createdFile)
      }
    )
  })
}

function readFile(_id) {
  const readstream = Attachment.readById(_id)
  return readstream
}

function readFileAsBuffer(_id) {
  return new Promise((resolve, reject) => {
    const bufs = []
    const readstream = Attachment.readById(_id)
    readstream.on('data', d => bufs.push(d))
    readstream.on('end', () => {
      resolve(Buffer.concat(bufs))
    })
    readstream.on('error', reject)
  })
}

module.exports = {
  writeFile,
  readFile,
  readFileAsBuffer
}
