const KYCModel = require('./models/MongoKycModel')
const FileStorage = require('./models/GridfsFileStorage')

module.exports = { KYCModel, FileStorage }
